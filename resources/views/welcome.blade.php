@extends('template')

@section('content')
    <ul class="list-group">
        <li class="list-group-item">
            <a href="{{action([\App\Http\Controllers\MainController::class,'index'])}}" aria-pressed="true">List objects</a>
        </li>
        <li class="list-group-item">
            <a href="{{action([\App\Http\Controllers\MainController::class,'create'])}}" aria-pressed="true">Create object</a>
        </li>
        <li class="list-group-item">
            <a href="{{action([\App\Http\Controllers\SearchController::class,'index'])}}" aria-pressed="true">Search objects</a>
        </li>
    </ul>

@endsection()