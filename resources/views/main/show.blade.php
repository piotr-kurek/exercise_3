@extends('template')

@section('content')
    <div class="card text-center">
        <div class="card-header">
            Object details
        </div>
        <div class="card-body">
            <h5 class="card-title">{{$main->name}}</h5>
            <p class="card-text">Current status: {{$main->status}}</p>
                <form action="{{action([\App\Http\Controllers\MainController::class,'edit'],[$main->id])}}" method="get">
                    @csrf
                    <button type="submit" name="id" value="{{$main->id}}" class="btn btn-primary btn-sm btn-block">Edit</button>
                </form>
                <form action="{{action([\App\Http\Controllers\MainController::class,'destroy'],[$main->id])}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" name="id" value="{{$main->id}}" class="btn btn-danger btn-sm btn-block">Delete</button>
                </form>
        </div>
        <div class="card-footer text-muted">
            @if($main->history->isEmpty())
                <p class="card-text">There were not changes to this object</p>
            @else
                <p class="card-text">Historical changes</p>
                @foreach($main->history as $history)
                    <p class="card-text">
                        @if($loop->first)
                            Initial status <strong>{{$history->status}}</strong> was changed at <strong>{{$history->created_at}}</strong>
                        @else
                            to status <strong>{{$history->status}}</strong> that subsequently was changed at <strong>{{$history->created_at}}</strong>
                        @endif
                    </p>
                @endforeach()
            @endif
        </div>
    </div>

@endsection()