@extends('template')

@section('content')
    <form action="{{action([\App\Http\Controllers\MainController::class,'store'])}}" method="post">
        @csrf

        @foreach($errors->default->all() as $error)
            <div class="alert alert-danger d-flex justify-content-center" role="alert">
                {{$error}}
            </div>
        @endforeach

        <div class="alert alert-primary d-flex justify-content-center">
            Create new main object
        </div>

        <div class="form-group">
            <label for="nameInput">Name:</label>
            <input type="text" class="form-control" name="name" id="nameInput">
        </div>
        <div class="form-group">
            <label for="statusInput">Status:</label>
            <select class="custom-select" id="statusInput" name="status">
                @foreach($statuses as $status)
                    <option
                            @if($loop->first)
                            selected
                            @endif
                            value="{{$status->name}}">{{$status->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <input type="submit" class="form-control" value="Create">
        </div>
    </form>
@endsection()