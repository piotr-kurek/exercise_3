@extends('template')

@section('content')
    <ul class="list-group">
        <li class="list-group-item list-group-item-primary">
            List of objects
        </li>
        @foreach($list as $item)
            <li class="list-group-item">
                <a href="{{action([\App\Http\Controllers\MainController::class,'show'],[$item->id])}}">
                    #{{$loop->index+1}}.
                    Object {{$item->name}}
                    with status {{$item->status}}
                </a>
            </li>
        @endforeach
        <li class="list-group-item list-group-item-primary">
            <a href="{{url('')}}">Return </a>
        </li>
    </ul>
@endsection()