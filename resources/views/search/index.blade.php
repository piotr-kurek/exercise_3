@extends('template')

@section('content')
    <form action="{{action([\App\Http\Controllers\SearchController::class,'query'])}}" method="post">
        @csrf
        @foreach($errors->default->all() as $error)
            <div class="alert alert-danger d-flex justify-content-center" role="alert">
                {{$error}}
            </div>
        @endforeach
        <div class="alert alert-primary d-flex justify-content-center">
            Search for objects
        </div>
        <div class="form-group">
            <label for="queryInput">Query:</label>
            <input type="text" class="form-control" name="query" id="queryInput">
        </div>
        <div class="form-group">
            <input type="submit" class="form-control btn-primary" value="Search">
        </div>
    </form>
@endsection()