<?php

namespace App\Http\Controllers;

use App\Http\Requests\Search\SearchRequest;
use App\Http\Resources\MainCollection;
use App\Models\Main;

class SearchController extends Controller
{

    public function index()
    {
        return view('search.index');
    }

    public function query(SearchRequest $request)
    {

        $input = $request->validated();
        $query = $input['query'];
        $list  = Main::search($query)->get();

        if ($request->expectsJson()) {
            return response()->json(new MainCollection($list));
        } else {
            return view('search.query', ['list' => $list]);
        }

    }
}
