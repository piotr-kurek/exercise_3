<?php

namespace App\Http\Controllers;

use App\Http\Requests\Main\StoreRequest;
use App\Http\Requests\Main\UpdateRequest;
use App\Http\Resources\MainCollection;
use App\Models\Main;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $list = Main::all();
        if ($request->expectsJson()) {
            return response()->json(new MainCollection($list));
        } else {
            return view('main.index', ['list' => $list]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $statuses = Status::all();
        return view('main.create', ['statuses' => $statuses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return void
     */
    public function store(StoreRequest $request)
    {
        //TODO modify if model name is guarded
        $main = Main::create($request->validated());
        if ($request->expectsJson()) {
            return response()->json($main);
        } else {
            return redirect()->route('main.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        if ($main = Main::find($id)) {
            if ($request->expectsJson()) {
                return response()->json($main);
            } else {
                return view('main.show', ['main' => $main]);
            }
        }
        return redirect()->route('main.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if ($main = Main::find($id)) {
            $statuses = Status::all();
            return view('main.edit', ['main' => $main, 'statuses' => $statuses]);
        }
        return redirect()->route('main.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return void
     */
    public function update(UpdateRequest $request, $id)
    {
        /** @var Main $main */
        if ($main = Main::find($id)) {
            $main->update($request->validated());
            $main->refresh();
            if ($request->expectsJson()) {
                return response()->json($main);
            } else {
                return redirect()->route('main.show', [$id]);
            }
        }
        if ($request->expectsJson()) {
            return response()->json(["error" => "not found"]);
        } else {
            return redirect()->route('main.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        /** @var Main $main */
        $main = Main::find($id);
        try {
            $main->delete();
        } catch (\Exception $exception) {
        }
        if ($request->expectsJson()) {
            return response()->json([]);
        } else {
            return redirect()->route('main.index');
        }
    }
}
