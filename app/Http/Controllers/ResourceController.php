<?php

namespace App\Http\Controllers;

use App\Models\Status;

class ResourceController extends Controller
{
    public function status()
    {
        return response()->json(Status::all());
        //TODO alternative as List
        //return response()->json(Status::all()->pluck(Status::NAME));
    }
}
