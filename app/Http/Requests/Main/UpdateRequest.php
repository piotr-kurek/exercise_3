<?php

namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //TODO update disabled by validation rules
//            'name'   => ['required', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255', 'exists:statuses,name']
        ];
    }
}
