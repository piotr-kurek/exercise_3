<?php


namespace App\Relations\BelongsTo;


trait Main
{
    public function main()
    {
        return $this->belongsTo(\App\Models\Main::class);
    }
}