<?php


namespace App\Relations\HasMany;


trait History
{
    public function history()
    {
        return $this->hasMany(\App\Models\History::class);
    }

}