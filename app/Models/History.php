<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Relations;


/**
 * Class History
 * @package App\Models
 * @property int $main_id
 * @property string $status
 * @property Main $main
 * @property Carbon $created_at
 */
class History extends Model
{
    use Relations\BelongsTo\Main;

    const MAIN_ID = 'main_id';

    const STATUS = 'status';

    const CREATED_AT = 'created_at';

    public $timestamps = false;

    protected $fillable = [
        self::STATUS
    ];

    protected $hidden = [
        self::MAIN_ID
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function (History $model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
