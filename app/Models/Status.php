<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 * @package App\Models
 * @property int $id
 * @property string $name
 */
class Status extends Model
{
    use HasFactory;

    const ID   = 'id';
    const NAME = 'name';

    public $timestamps = false;

    protected $casts = [
        self::ID => 'int',
    ];

    protected $fillable = [
        self::NAME
    ];

    protected $visible = [
        self::NAME
    ];
}
