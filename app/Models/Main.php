<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Relations;
use Illuminate\Support\Collection;
use Laravel\Scout\Searchable;

/**
 * Class Main
 * @package App\Models
 * @property int $id
 * @property string $name;
 * @property string $status
 * @property Collection|History[] $history
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Main extends Model
{
    use Searchable;

    use Relations\HasMany\History;

    const ID = 'id';

    const NAME = 'name';

    const STATUS = 'status';

    const HISTORY = 'history';

    protected $fillable = [
        //TODO modify if model name is guarded (notice storage definition in controller)
        self::NAME,
        self::STATUS
    ];

    protected $hidden = [
        self::UPDATED_AT
    ];

    protected $with = [
        self::HISTORY
    ];

    public static function boot()
    {
        parent::boot();

        static::updating(function (Main $model) {
            $model->history()->create([History::STATUS => $model->getOriginal(self::STATUS)]);
        });

        static::deleting(function (Main $model) {
            $model->history()->delete();
        });
    }

}
