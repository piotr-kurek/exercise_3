<?php

use App\Models\Main;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('temp', function () {
    /** @var Main $main */
    $main = Main::firstOrCreate([
        Main::NAME => 'A',
    ],
        [
            Main::STATUS => '1',
        ]);
    $main->status++;
    $main->save();
    $main->status++;
    $main->save();
    $main->refresh();
    dd($main->toArray());
})->purpose('Test');
