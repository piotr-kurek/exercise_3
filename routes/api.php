<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('main', [MainController::class, 'index']);
Route::post('main', [MainController::class, 'store']);
Route::get('main/{id}', [MainController::class, 'show']);
Route::patch('main/{id}', [MainController::class, 'update']);
Route::delete('main/{id}', [MainController::class, 'destroy']);

Route::get('status', [ResourceController::class, 'status']);

Route::match(['get', 'post'], 'query', [SearchController::class, 'query']);